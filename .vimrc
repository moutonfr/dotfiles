""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"   Install Plug first
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

call plug#begin('~/.vim/plugged')

" airline status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Color Schemes/Change vim colors easily
Plug 'flazz/vim-colorschemes'

" CtrlP
Plug 'kien/ctrlp.vim'

" vim-unimpaired
Plug 'tpope/vim-unimpaired'


call plug#end()

filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim stetting
"

" Color
set t_Co=256

" tabs
set tabstop=2
set shiftwidth=2
set expandtab

" line number
set number
set relativenumber

" windows
if has("gui_running")
  set guifont=DejaVu_Sans_Mono_for_Powerline:h10:cANSI:qDRAFT
  set encoding=utf-8
  " GUI is running or is about to start.
  " Maximize gvim window (for an alternative on Windows, see simalt below).
  set lines=999 columns=999
endif

" background color redrawing correctly
if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
    set t_ut=
endif

" Clipboard
set clipboard^=unnamedplus,unnamed
 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Moving around
"
" Faster window switching
nmap <c-j> <c-w>j
nmap <c-k> <c-w>k
nmap <c-l> <c-w>l
nmap <c-h> <c-w>h

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins settings
"
"

" Airline Configuration
"
" Always display status line
set laststatus=2

" Auto populate the g:airline_symbols dictionary
let g:airline_powerline_fonts = 1
let g:airline_theme='papercolor'

" Set the color scheme
set background=dark
let g:solarized_termcolors=256
let g:lucius_style = 'light'
let g:lucius_contrast = 'high'
colorscheme lucius

" CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" vim-unimpaired extensions
nmap [g :tabprev<return>
nmap ]g :tabnext<return>

